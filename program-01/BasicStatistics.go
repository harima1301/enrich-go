package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
)

func Statistics() {
	var max float64
	var min float64
	var sum float64
	argSlice := os.Args[1:]
	length := len(argSlice)
	fmt.Println(length)
	sort.Slice(argSlice, func(i int, j int) bool {
		return argSlice[i] < argSlice[j]
	})
	if r, err := strconv.ParseFloat(argSlice[0], 64); err == nil {
		max = r
		min = r
		sum = r
	}

	for i := 1; i < len(argSlice); i++ {
		if r, err := strconv.ParseFloat(argSlice[i], 64); err == nil {
			if r > max {
				max = r
			}
			if r < min {
				min = r
			}
			sum += r

		}
	}

	fmt.Printf("Min is: %v\n", min)
	fmt.Printf("Max is: %v\n", max)
	fmt.Printf("Mean is %.2f\n", sum/float64(length))

	if length%2 != 0 {
		fmt.Printf("Median is %v\n", argSlice[length/2])
	} else {
		i := length / 2
		v1, err1 := strconv.ParseFloat(argSlice[i], 64)
		v2, err2 := strconv.ParseFloat(argSlice[i-1], 64)
		if err1 == nil && err2 == nil {
			fmt.Printf("Median is %2.f\n", (v1+v2)/2)
		}

	}

}

func main() {
	Statistics()
}
