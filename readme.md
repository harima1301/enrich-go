# Challange-01 Guilde

## Hướng dẫn build executable file(.exe) trước khi chạy chương trình.

Chạy lệnh: `go env -w GOBIN=~/path/directory/bin` với `~/path/directory/bin` là nơi muốn chứa các file .exe. Đến các folder chứa file .go muốn compile.
Chạy lệnh go install file.go, với file.go là file code muốn compile.

## Hướng dẫn add path cho cac .exe file
Nếu muốn chạy file ở bất kì directory nào thì cần add `~/path/directory/bin` vào `PATH`<br />
Ở command line chạy: `export PATH=~/path/directory/bin:$PATH`
## Danh sách các thử thách

### Chương trình 01
Chương trình GO nhận danh sách các số (hợp lệ) từ tham số dòng lệnh (command-line argument), thực hiện tính toán và hiển thì ra màn hình các thông tin sau:

Số nhỏ nhất, số lớn nhất (min, max)
Giá trị trung bình (mean)
Giá trị trung vị (median)<br />
Ví dụ:<br />
`BasicStatistics 5 4 3 8 9`<br />

Kết quả:
```console
5
Min is: 3
Max is: 9
Mean is 5.80
Median is 5
```

### Chương trình 02
Viết chương trình GO nhận một số nguyên từ tham số dòng lệnh. Số này có tối đa 3 chữ số. Chương trình hiển thị dạng văn bản của số đó.<br />
Ví dụ:<br />
`ReadNumbers 102`<br />
Kết quả:
```console
Một Trăm Lẻ Hai
```

### Chương trình 03
 Viết chương trình sinh ra bảng dữ liệu ngẫu nhiên từ các tham số dòng lệnh có dạng như sau:
Số_dòng  tên_cột_1  kiểu_dữ_liệu_1   tên_cột_2  kiểu_dữ_liệu_2 …<br />
Ví dụ:<br />
`RandomData 4 ten String tuoi Int`<br />
```console
Cột ten có giá trị: [qSIbAHLFVrSXHke nmZpKdP asSwTPnFTvXZx cBIHdex]
Cột tuoi có giá trị: [7110614231151869959 2797044595545675541 48374097636854899 389117428805443335]
```

### Chương trình 04
Viết chương trình thực hiện cộng 2 chuỗi<br />
Ví dụ:<br />
`sum 129 989`<br />
Kết quả:
```console
Các bước thực hiện:
Lấy '9' cộng '9' => Kết quả được 18, lưu 1, còn 8
Lấy '2' cộng '8' => Kết quả được 10, cộng với nhớ 1 được 11, lưu 1, còn 1
Lấy '1' cộng '9' => Kết quả được 10, cộng với nhớ 1 được 11
Kết quả cuối cùng là: 1118
```

### Chương trình 05
Viết chương trình để tính tổng số ngày giữa 2 ngày được tuyền từ tham số dòng lệnh.<br />
Ví dụ:<br />
`CountDate 2019-12-31  2020-12-31`<br />
Kết quả:
```console
Số ngày từ 2019-12-31 đến 2020-12-31 là: 366
```