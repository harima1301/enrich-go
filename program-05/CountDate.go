package main

import (
	"fmt"
	"os"
	"time"
)

func CountDate(start string, end string) {
	timeStart, _ := time.Parse("2006-01-02", start)
	var timeEnd time.Time
	if end != "" {
		timeEnd, _ = time.Parse("2006-01-02", end)
	} else {
		timeEnd = time.Now()
	}
	date := int(timeEnd.Sub(timeStart).Hours()) / 24
	fmt.Printf("Số ngày từ %v đến %v là: %v\n", timeStart.String()[0:10], timeEnd.String()[0:10], date)
}

func main() {
	input := os.Args[1:]
	CountDate(input[0], input[1])
}
