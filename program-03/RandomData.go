package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func RandomData(argSlice []string) {
	RowLength, error := strconv.ParseInt(argSlice[0], 10, 0)
	var table map[string]interface{} = make(map[string]interface{})
	rand.Seed(time.Now().Unix())
	if error == nil {
		for i := 1; i < len(argSlice); i += 2 {
			var temp []interface{} = make([]interface{}, 0)
			switch argSlice[i+1] {
			case "Bool":
				for j := 0; j < int(RowLength); j++ {
					temp = append(temp, rand.Float32() < 0.5)
				}
			case "Int":
				for j := 0; j < int(RowLength); j++ {
					temp = append(temp, rand.Int())
				}
			case "Uint":
				for j := 0; j < int(RowLength); j++ {
					temp = append(temp, rand.Uint32())
				}
			case "Float64":
				for j := 0; j < int(RowLength); j++ {
					temp = append(temp, rand.Float64())
				}
			case "Complex":
				for j := 0; j < int(RowLength); j++ {
					temp = append(temp, complex(rand.Float64(), rand.Float64()))
				}
			case "String":
				charset := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
				for j := 0; j < int(RowLength); j++ {
					var str string = ""
					stringLength := rand.Intn(20)
					for l := 0; l < stringLength; l++ {
						randChar := charset[rand.Intn(len(charset))]
						str += string(randChar)
					}
					temp = append(temp, str)
				}
			}
			if temp != nil {
				table[argSlice[i]] = temp
			} else {
				fmt.Println("Data Type is not valid, the column don't add to table")
			}
		}

	}
	for k, v := range table {
		fmt.Printf("Cột %v có giá trị: %v\n", k, v)
	}

}

func main() {
	argSlice := os.Args[1:]
	RandomData(argSlice)
}
