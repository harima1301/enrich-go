package main

import (
	"fmt"
	"os"
)

func sum(st1 string, st2 string) {
	for _, v := range st1 {
		if v < '0' || v > '9' {
			fmt.Println("String contains not digit character")
			return
		}
	}

	for _, v := range st1 {
		if v < '0' || v > '9' {
			fmt.Println("String contains not digit character")
			return
		}
	}

	if len(st1) == 0 {
		st1 = "0"
	}

	if len(st2) == 0 {
		st2 = "0"
	}

	for st1[0] == '0' && len(st1) > 1 {
		st1 = st1[1:]
	}

	for st2[0] == '0' && len(st2) > 1 {
		st2 = st2[1:]
	}

	var sum, remain, save, preSave, maxlen int
	var charst1, charst2 byte
	var result string = ""
	lenst1 := len(st1)
	lenst2 := len(st2)
	if lenst1 >= lenst2 {
		maxlen = lenst1
	} else {
		maxlen = lenst2
	}
	fmt.Println("Các bước thực hiện:")
	for i := 0; i < maxlen; i++ {
		indexst1 := lenst1 - i - 1
		indexst2 := lenst2 - i - 1
		if indexst1 >= 0 {
			charst1 = st1[indexst1]
		} else {
			charst1 = '0'
		}
		if indexst2 >= 0 {
			charst2 = st2[indexst2]
		} else {
			charst2 = '0'
		}
		//Convert int32 to int
		sum = int(charst1-'0'+charst2-'0') + save
		preSave = save
		remain = sum % 10
		save = sum / 10
		result = fmt.Sprintf("%v%v", remain, result)
		if i != maxlen-1 {
			if preSave == 0 {
				fmt.Printf("Lấy '%v' cộng '%v' => Kết quả được %v, lưu %v, còn %v\n", string(charst1), string(charst2), sum, save, remain)
			} else {
				fmt.Printf("Lấy '%v' cộng '%v' => Kết quả được %v, cộng với nhớ %v được %v, lưu %v, còn %v\n", string(charst1), string(charst2), int(charst1-'0'+charst2-'0'), preSave, sum, save, remain)
			}
		} else {
			fmt.Printf("Lấy '%v' cộng '%v' => Kết quả được %v, cộng với nhớ %v được %v\n", string(charst1), string(charst2), int(charst1-'0'+charst2-'0'), preSave, sum)
		}
	}

	if save != 0 {
		result = fmt.Sprintf("%v%v", save, result)
	}

	fmt.Printf("Kết quả cuối cùng là: %v\n", result)
}

func main() {
	// notHaveInput := true
	// for notHaveInput {
	// 	reader := bufio.NewReader(os.Stdin)
	// 	// fmt.Scanf("%s %s %s", &funcName, &input1, &input2)
	// 	ConsoleInput, _ := reader.ReadString('\n')
	// 	ConsoleInput = strings.TrimSuffix(ConsoleInput, "\n")
	// 	input = strings.Split(ConsoleInput, " ")
	// 	if len(input) == 2 {
	// 		notHaveInput = false
	// 	} else {
	// 		fmt.Println("Wrong parameter, please enter again")
	// 	}
	// }
	input := os.Args[1:]
	sum(input[0], input[1])
}
