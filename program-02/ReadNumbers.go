package main

import (
	"fmt"
	"os"
)

func ReadNumber(argNum string) {
	Result := ""
	if len(argNum) > 3 {
		fmt.Println("Number must have 3 character or less")
		return
	}
	NumberMap := map[rune]string{'0': "Lẻ", '1': "Một", '2': "Hai", '3': "Ba", '4': "Bốn", '5': "Năm", '6': "Sáu", '7': "Bảy", '8': "Tám", '9': "Chín"}
	for i, v := range argNum {

		//Regular logic
		// if (v == '0' && i != 1) || (v == '1' && i == 1) {

		// } else {
		// 	Result += NumberMap[v] + " "
		// }

		//apply demorgan's law
		if (v != '0' || i == 1) && (v != '1' || i != 1) {
			Result += NumberMap[v] + " "
		}

		// switch v {
		// case '0':
		// 	if i == 1 {
		// 		Result += "Lẻ "
		// 	}
		// case '1':
		// 	if i != 1 {
		// 		Result += "Một "
		// 	}
		// case '2':
		// 	Result += "Hai "
		// case '3':
		// 	Result += "Ba "
		// case '4':
		// 	Result += "Bốn "
		// case '5':
		// 	Result += "Năm "
		// case '6':
		// 	Result += "Sáu "
		// case '7':
		// 	Result += "Bảy "
		// case '8':
		// 	Result += "Tám "
		// case '9':
		// 	Result += "Chín "
		// }
		switch i {
		case 0:
			if v != 48 {
				Result += "Trăm "
			}
		case 1:
			if v != '0' {
				if v == '1' {
					Result += "Mười "
				} else {
					Result += "Mươi "
				}
			}
		}
	}
	fmt.Println(Result)
}

func main() {
	argNum := os.Args[1]
	ReadNumber(argNum)
}
